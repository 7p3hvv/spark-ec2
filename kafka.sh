#!/bin/bash
source ~/.bash_profile
echo 'install kafka'
wget https://www-us.apache.org/dist/kafka/0.10.2.1/kafka_2.12-0.10.2.1.tgz
tar -xzf kafka_2.12-0.10.2.1.tgz
rm kafka_2.12-0.10.2.1.tgz
cd kafka_2.12-0.10.2.1
bin/zookeeper-server-start.sh config/zookeeper.properties 2>&1 1>/dev/null&
sleep 10
bin/kafka-server-start.sh config/server.properties 2>&1 1>/dev/null &
echo delete.topic.enable=true >> config/server.properties;
cd ..
echo 'kafka was installed and is ready to use'
