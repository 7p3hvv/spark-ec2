#!/bin/bash
sudo yum install -q -y java-1.8.0-openjdk-devel gcc gcc-c++ ant git

sudo yum --enablerepo='*-debug*' install -q -y java-1.8.0-openjdk-debuginfo.x86_64
yum install -q -y httpd24

pushd /root/spark-ec2 > /dev/null
source ec2-variables.sh
echo "$MASTERS" > masters
echo "$SLAVES" > slaves
popd > /dev/null

echo '#!/bin/bash' > /usr/bin/realpath
echo 'readlink -e "$@"' >> /usr/bin/realpath
chmod a+x /usr/bin/realpath

echo '''#!/bin/bash
yum groupinstall -q -y  'Development Tools'
yum install -q -y binutils  openssl-devel
yum install -q -y sqlite-devel
wget https://www.python.org/ftp/python/3.6.1/Python-3.6.1.tar.xz
tar xJf Python-3.6.1.tar.xz
cd Python-3.6.1
echo configure python
./configure --with-ensurepip=install --prefix=/usr --enable-loadable-sqlite-extensions  1>/dev/null 2>/dev/null
echo install 
sudo make -j$(nproc) install 1>/dev/null 2>/dev/null''' > install_python3.6.sh
echo sed -i \'s/^all:.*$/all: build_all/\' Makefile >> install_python3.6.sh
echo '''/root/spark-ec2/copy-dir `realpath .`
cd ../
rm -fr Python-3.6.1*
python3.6 -m pip install --upgrade pip
python3.6 -m pip install ipython
python3.6 -m pip install py4j
python3.6 -m pip install pandas happybase praw 
python3.6 -m pip install nltk
python3.6 -m nltk.downloader -d /usr/local/share/nltk_data stopwords
rm /usr/bin/python
ln -s /usr/bin/python3.6 /bin/python
rm install_python3.6.sh''' >> install_python3.6.sh 

if ! [[ -a /usr/bin/python3.6 ]]; then
    bash install_python3.6.sh
else 
    rm install_python3.6.sh
fi

if ! [[ -a /usr/lib/librdkafka.so.1 ]]; then
    wget https://github.com/edenhill/librdkafka/archive/v0.9.5.tar.gz
    tar xzf v0.9.5.tar.gz
    cd librdkafka-0.9.5
    ./configure --prefix=/usr
    make -j$(nproc)  2>&1 1>/dev/null 
    make install  2>&1 1>/dev/null
    cd ..
    rm -fr *0.9.5*
fi
python3.6 -m pip install confluent_kafka

echo '''#!/bin/bash
pkgname=p7zip
pkgver=16.02
pkgrel=3
wget https://downloads.sourceforge.net/project/$pkgname/$pkgname/$pkgver/${pkgname}_${pkgver}_src_all.tar.bz2
bzip2 -d ${pkgname}_${pkgver}_src_all.tar.bz2
tar xf ${pkgname}_${pkgver}_src_all.tar
cd ${pkgname}_${pkgver} 
make all3 -j4 2>&1 1>/dev/null
/root/spark-ec2/copy-dir `realpath .`
make install DEST_HOME=/usr   2>&1 1>/dev/null
cd ..
rm -fr *${pkgname}_${pkgver}*
rm install_p7zip.sh''' > install_p7zip.sh

if ! [[ -a /usr/bin/7z ]]; then
    bash install_p7zip.sh
else
    rm install_p7zip.sh
fi



yum install -q -y  nginx

if ! [[ -a /usr/lib/python3.6/site-packages/bokeh-0.12.6.dev3-py3.6.egg ]]; then
    curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.2/install.sh | bash
    export NVM_DIR="$HOME/.nvm"
    [ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh" # This loads nvm
    wget https://github.com/bokeh/bokeh/archive/0.12.6dev3.tar.gz
    tar xzf 0.12.6dev3.tar.gz
    cd bokeh-0.12.6dev3
    cd bokehjs
    nvm install v7.9.0  
    echo npm install
    npm install 2>&1 1>/dev/null
    cd ..
    echo bokeh setup.by 
    python3.6 setup.py --build-js install 2>&1 1>/dev/null
    cd ..
    rm -fr *0.12.6dev3*
fi   



echo "export JAVA_HOME=/usr/lib/jvm/java-1.8.0" >> ~/.bash_profile
echo "export PYSPARK_PYTHON=python3.6" >> ~/.bash_profile
echo "export PYSPARK_DRIVER_PYTHON=ipython3" >> ~/.bash_profile
SPARK_HOME=/root/spark
echo "export SPARK_HOME=$SPARK_HOME" >> ~/.bash_profile
echo "export PATH=$PATH:$SPARK_HOME/bin" >> ~/.bash_profile
source ~/.bash_profile

if [[ -a /usr/bin/python ]]; then
    rm /usr/bin/python
fi
ln -s /usr/bin/python3.6 /usr/bin/python

