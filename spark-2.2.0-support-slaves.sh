#!/bin/bash
sudo yum install -q -y java-1.8.0-openjdk-devel gcc gcc-c++ ant git

sudo yum --enablerepo='*-debug*' install -q -y java-1.8.0-openjdk-debuginfo.x86_64
yum install -q -y httpd24

echo '#!/bin/bash' > /usr/bin/realpath
echo 'readlink -e "$@"' >> /usr/bin/realpath

echo '''#!/bin/bash
yum groupinstall -q -y  'Development Tools'
yum install -q -y binutils  openssl-devel
yum install -q -y sqlite-devel
if ! [[ -a /root/Python-3.6.1 ]]; then
    wget https://www.python.org/ftp/python/3.6.1/Python-3.6.1.tar.xz
    tar xJf Python-3.6.1.tar.xz
    cd Python-3.6.1
    echo configure python
    ./configure --with-ensurepip=install --prefix=/usr --enable-loadable-sqlite-extensions  1>/dev/null 2>/dev/null
    cd ../
fi
cd Python-3.6.1
echo install 
sudo make -j$(nproc) install 1>/dev/null 2>/dev/null
cd ../
rm -fr Python-3.6.1*
python3.6 -m pip install --upgrade pip
python3.6 -m pip install ipython
python3.6 -m pip install py4j
python3.6 -m pip install pandas
python3.6 -m pip install nltk
python3.6 -m nltk.downloader -d /usr/local/share/nltk_data stopwords
rm /usr/bin/python
ln -s /usr/bin/python3.6 /usr/bin/python
rm install_python3.6.sh''' > install_python3.6.sh 

if ! [[ -a /usr/bin/python3.6 ]]; then
    bash install_python3.6.sh
else 
    rm install_python3.6.sh
fi


echo '''#!/bin/bash
pkgname=p7zip
pkgver=16.02
pkgrel=3
if ! [[ -a /root/${pkgname}_${pkgver} ]]; then
    wget https://downloads.sourceforge.net/project/$pkgname/$pkgname/$pkgver/${pkgname}_${pkgver}_src_all.tar.bz2
    bzip2 -d ${pkgname}_${pkgver}_src_all.tar.bz2
    tar xf ${pkgname}_${pkgver}_src_all.tar
    cd ${pkgname}_${pkgver} 
    echo make  ${pkgname}_${pkgver} 
    make all3 -j4 2>&1 1>/dev/null
    cd ..
fi
cd ${pkgname}_${pkgver} 
make install DEST_HOME=/usr   2>&1 1>/dev/null
cd ..
rm -fr *${pkgname}_${pkgver}*
rm install_p7zip.sh''' > install_p7zip.sh

if ! [[ -a /usr/bin/7z ]]; then
    bash install_p7zip.sh
else
    rm install_p7zip.sh
fi

echo "export JAVA_HOME=/usr/lib/jvm/java-1.8.0" >> ~/.bash_profile
echo "export PYSPARK_PYTHON=python3.6" >> ~/.bash_profile
echo "export PYSPARK_DRIVER_PYTHON=ipython3" >> ~/.bash_profile
SPARK_HOME=/root/spark
echo "export SPARK_HOME=$SPARK_HOME" >> ~/.bash_profile
echo "export PATH=$PATH:$SPARK_HOME/bin" >> ~/.bash_profile
source ~/.bash_profile
